import logging

import pytest

from pyliblorawan.devices.browan.tbms100 import TBMS100
from pyliblorawan.models import Uplink


@pytest.mark.asyncio
async def test_parse_port_102(caplog: pytest.LogCaptureFixture):
    device = TBMS100()
    payload = "000B374DB77A0400"
    uplink = Uplink("FEEDABCD00000002", bytes.fromhex(payload), 102)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_pir_status": False,
        "_battery_level": 3.6,
        "_temperature": 23,
        "_time_since_last_event": 2815500,
        "_total_event_counter": 1146,
    }

    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_unknown(caplog: pytest.LogCaptureFixture):
    device = TBMS100()
    payload = "000B374DB77A0400"
    uplink = Uplink("FEEDABCD00000002", bytes.fromhex(payload), 101)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {}
    assert caplog.record_tuples == [
        (
            "pyliblorawan.devices.browan.tbms100",
            logging.WARNING,
            'Unknown frame port "101"',
        )
    ]
