import logging

import pytest

from pyliblorawan.devices.browan.tbhh100 import TBHH100
from pyliblorawan.models import Uplink


@pytest.mark.asyncio
async def test_parse_port_107(caplog: pytest.LogCaptureFixture):
    device = TBHH100()
    payload = "080b2859ffffffff"
    uplink = Uplink("FEEDABCD00000002", bytes.fromhex(payload), 107)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_battery_level": 3.6,
        "_temperature": 8,
        "_humidity": 89,
    }

    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_unknown(caplog: pytest.LogCaptureFixture):
    device = TBHH100()
    payload = "000B374DB77A0400"
    uplink = Uplink("FEEDABCD00000002", bytes.fromhex(payload), 101)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {}
    assert caplog.record_tuples == [
        (
            "pyliblorawan.devices.browan.tbhh100",
            logging.WARNING,
            'Unknown frame port "101"',
        )
    ]


@pytest.mark.asyncio
async def test_voc_not_supported(caplog: pytest.LogCaptureFixture):
    device = TBHH100()
    payload = "000b2859ffffffff"
    uplink = Uplink("FEEDABCD00000002", bytes.fromhex(payload), 107)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_battery_level": 3.6,
        "_temperature": 8,
        "_humidity": 89,
    }

    assert caplog.record_tuples == [
        (
            "pyliblorawan.devices.browan.tbhh100",
            logging.ERROR,
            'TBHH100 parse does not implement CO2eq/VOC. Received payload "000b2859ffffffff"',
        )
    ]
