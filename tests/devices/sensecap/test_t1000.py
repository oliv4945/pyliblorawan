import asyncio
import datetime
import logging
import os

import pytest
from aioresponses import CallbackResult, aioresponses
from yarl import URL

from pyliblorawan.devices.sensecap.t1000 import T1000, aiohttp_session
from pyliblorawan.models import LocationSources, Uplink, WifiMacAddress

os.environ["PYLIBLORAWAN_LORA_CLOUD_TOKEN"] = "TEST-API-TOKEN"


def callback_loracloud_lorawifi_ok(url: URL, **kwargs) -> CallbackResult:
    assert url == URL("https://mgs.loracloud.com/api/v1/solve/loraWifi")
    assert kwargs["headers"]["authorization"] == "TEST-API-TOKEN"
    assert kwargs["headers"]["accept"] == "application/json"
    assert kwargs["headers"]["content-type"] == "application/json"

    return CallbackResult(
        payload={
            "result": {
                "latitude": 12.3456,
                "longitude": 98.76543,
                "altitude": 0.0,
                "accuracy": 1234,
                "algorithmType": "Wifi",
                "numberOfGatewaysReceived": 0,
                "numberOfGatewaysUsed": 0,
            },
            "warnings": [""],
            "errors": [],
        }
    )


@pytest.mark.asyncio
async def test_parse_port_5_device_status_event(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "0153010501040207001e00050005010000001e000500016801012c000005001e025800000000000500010064000000"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_battery": 83.0,
        "_interval_event": 300,
        "_interval_heartbeat": 1800,
        "_interval_periodic": 300,
        "_version_hardware": "1.4",
        "_version_software": "1.5",
        "sensecap_t1000_event_mode_light": False,
        "sensecap_t1000_event_mode_motion": False,
        "sensecap_t1000_event_mode_motionless": False,
        "sensecap_t1000_event_mode_shock": True,
        "sensecap_t1000_event_mode_temperature": False,
        "sensecap_t1000_interval_motion_start": 300,
        "sensecap_t1000_interval_periodic_light_exceeded": 300,
        "sensecap_t1000_interval_periodic_temperature_exceeded": 300,
        "sensecap_t1000_interval_sample_light": 1,
        "sensecap_t1000_interval_sample_temperature": 30,
        "sensecap_t1000_positioning_strategy": "Bluetooth+Wi-Fi+GNSS",
        "sensecap_t1000_sensors_enabled": True,
        "sensecap_t1000_sos_mode": "single",
        "sensecap_t1000_threshold_accelerometer_motion": 0.03,
        "sensecap_t1000_threshold_accelerometer_shock": 0.3,
        "sensecap_t1000_threshold_light_max": 100,
        "sensecap_t1000_threshold_light_min": 0,
        "sensecap_t1000_threshold_light_rule": "light ≤ min threshold",
        "sensecap_t1000_threshold_temperature_max": 60.0,
        "sensecap_t1000_threshold_temperature_min": 0.0,
        "sensecap_t1000_threshold_temperature_rule": "temperature ≤ min threshold",
        "sensecap_t1000_timeout_motionless": 21600,
        "sensecap_t1000_working_mode": "event",
    }

    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_5_device_status_periodic(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "025601050104010002d0003c003f0000"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_battery": 86.0,
        "_interval_event": 3780,
        "_interval_heartbeat": 43200,
        "_interval_periodic": 3600,
        "_version_software": "1.5",
        "_version_hardware": "1.4",
        "sensecap_t1000_positioning_strategy": "GNSS",
        "sensecap_t1000_sensors_enabled": False,
        "sensecap_t1000_sos_mode": "single",
        "sensecap_t1000_working_mode": "periodic",
    }

    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_5_heartbeat(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "0564010001"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_battery": 100.0,
        "sensecap_t1000_positioning_strategy": "GNSS",
        "sensecap_t1000_sos_mode": "continuous",
        "sensecap_t1000_working_mode": "periodic",
    }

    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_5_location_error(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "0D00000004"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {"sensecap_t1000_location_error": "BLE timeout"}
    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_5_gnss_only(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "09000000006463186806ca506801587e4c56"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_latitude": 22.576716,
        "_longitude": 113.922152,
        "_battery": 86.0,
        "_location_source": LocationSources.GNSS,
        "_timestamp": datetime.datetime(2023, 5, 16, 5, 45, 12),
        "sensecap_t1000_location_trigger": "no_event",
        "sensecap_t1000_motion_segment_number": 0,
    }

    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_5_gnss_sensors(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "06000008006462248d06ca502801587ec600fe000057"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_latitude": 22.576838,
        "_longitude": 113.922088,
        "_battery": 87.0,
        "_location_source": LocationSources.GNSS,
        "_temperature": 25.4,
        "_timestamp": datetime.datetime(2023, 5, 15, 12, 24, 45),
        "sensecap_t1000_light_%": 0,
        "sensecap_t1000_location_trigger": "shock",
        "sensecap_t1000_motion_segment_number": 0,
    }

    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_5_status_sensors(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "11030000006571b5f000dc00005e"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_battery": 94.0,
        "_temperature": 22.0,
        "_timestamp": datetime.datetime(2023, 12, 7, 12, 9, 20),
        "sensecap_t1000_event_status": "movement_start",
        "sensecap_t1000_light_%": 0,
        "sensecap_t1000_positioning_status": "Wi-Fi+GNSS scan timeout",
    }

    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_5_wifi_only(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = (
        "0A0000890064622472487397162234bb3ccd5798fd2ebc74cf002f3ad0a9FFFFFFFFFFFFb957"
    )
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)
    global aiohttp_session

    with aioresponses() as m:
        m.post(
            "https://mgs.loracloud.com/api/v1/solve/loraWifi",
            callback=callback_loracloud_lorawifi_ok,
        )
        await device.parse_uplink(uplink)

    assert caplog.record_tuples == []
    assert uplink.sensors.__dict__ == {
        "_battery": 87.0,
        "_latitude": 12.3456,
        "_location_source": LocationSources.WIFI,
        "_longitude": 98.76543,
        "_timestamp": datetime.datetime(2023, 5, 15, 12, 24, 18),
        "sensecap_t1000_location_trigger": "button;movement_start;shock",
        "sensecap_t1000_mac_addresses": [
            WifiMacAddress(mac="487397162234", rssi=-69),
            WifiMacAddress(mac="3CCD5798FD2E", rssi=-68),
            WifiMacAddress(mac="74CF002F3AD0", rssi=-87),
        ],
        "sensecap_t1000_motion_segment_number": 0,
    }


@pytest.mark.asyncio
async def test_parse_port_5_wifi_sensors(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "070000080064622472487397162234bb3ccd5798fd2ebc74cf002f3ad0a9ec26ca022958b900fe000057"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)
    global aiohttp_session

    with aioresponses() as m:
        m.post(
            "https://mgs.loracloud.com/api/v1/solve/loraWifi",
            callback=callback_loracloud_lorawifi_ok,
        )
        await device.parse_uplink(uplink)

    assert caplog.record_tuples == []
    assert uplink.sensors.__dict__ == {
        "_battery": 87.0,
        "_latitude": 12.3456,
        "_location_source": LocationSources.WIFI,
        "_longitude": 98.76543,
        "_temperature": 25.4,
        "_timestamp": datetime.datetime(2023, 5, 15, 12, 24, 18),
        "sensecap_t1000_light_%": 0,
        "sensecap_t1000_location_trigger": "shock",
        "sensecap_t1000_mac_addresses": [
            WifiMacAddress(mac="487397162234", rssi=-69),
            WifiMacAddress(mac="3CCD5798FD2E", rssi=-68),
            WifiMacAddress(mac="74CF002F3AD0", rssi=-87),
            WifiMacAddress(mac="EC26CA022958", rssi=-71),
        ],
        "sensecap_t1000_motion_segment_number": 0,
    }


device_status_unknowns = [
    (
        "0153010501040207001e00050005010000001e000500016801012c000005001e025800000400000500010064000000",
        'Unknown temperature threshold rule "4"',
    ),
    (
        "0153010501040207001e00050005010000001e000500016801012c000005001e025800000000000500010064000004",
        'Unknown light threshold rule "4"',
    ),
    (
        "025601050104010802d0003c003f0000",
        'Unknown positioning strategy "8"',
    ),
    (
        "025601050104010002d0003c003f0002",
        'Unknown SOS mode "2"',
    ),
    (
        "025601050104030002d0003c003f0000",
        'Unknown working mode "3"',
    ),
    (
        "12",
        'Unknown data id "18"',
    ),
    ("0D00000008", 'Unknown location error code "8"'),
    ("11030000086571b5f000dc00005e", 'Unknown event status "8"'),
    ("110F0000006571b5f000dc00005e", 'Unknown positioning status "15"'),
]


@pytest.mark.parametrize("payload,error_message", device_status_unknowns)
@pytest.mark.asyncio
async def test_parse_port_5_device_status_unknowns(
    caplog: pytest.LogCaptureFixture, payload: str, error_message: str
):
    device = T1000()
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert caplog.record_tuples == [
        (
            "pyliblorawan.devices.sensecap.t1000",
            logging.WARNING,
            error_message,
        )
    ]


@pytest.mark.asyncio
async def test_parse_port_unknown(caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "000B374DB77A0400"
    uplink = Uplink("FEEDABCD00000002", bytes.fromhex(payload), 101)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {}
    assert caplog.record_tuples == [
        (
            "pyliblorawan.devices.sensecap.t1000",
            logging.WARNING,
            'Unknown frame port "101"',
        )
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize("port", range(192, 200))
async def test_loracloud_not_supported(port: int, caplog: pytest.LogCaptureFixture):
    device = T1000()
    payload = "000B374DB77A0400"
    uplink = Uplink("FEEDABCD00000002", bytes.fromhex(payload), port)

    with pytest.raises(NotImplementedError):
        await device.parse_uplink(uplink)
    assert caplog.record_tuples == []
