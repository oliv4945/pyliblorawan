import logging

import pytest

from pyliblorawan.devices.tektelic.vivid import Vivid
from pyliblorawan.models import Uplink


@pytest.mark.asyncio
async def test_parse_port_5(caplog_debug: pytest.LogCaptureFixture):
    device = Vivid()
    payload = "00"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {}
    assert caplog_debug.record_tuples == [
        (
            "pyliblorawan.devices.tektelic.vivid",
            logging.WARNING,
            "System diagnostics parsing not implemented",
        )
    ]


parse_port_10_data = [
    (
        "03 67 00 0A 04 68 28",
        {
            "_temperature": 1,
            "_humidity": 20,
        },
    ),
    (
        "04 68 14 01 00 FF 08 04 00 05",
        {"_humidity": 10.0, "_magnet_detected": False, "_total_event_counter": 0},
    ),
    (
        "04 68 2A 03 67 FF FF 00 BA 0B B8",
        {"_battery_level": 3.0, "_humidity": 21.0, "_temperature": -0.1},
    ),
    (
        "02 00 FF 0E 00 00",
        {"_light_detected": True, "tektelic_vivid_input_state": True},
    ),
    ("0D 04 00 02", {"_total_event_counter": 0}),
    (
        "05021234 0771234534564567 0C00FF 0A00FF 00FF012C",
        {
            "_battery_level": 3.0,
            "_acceleration": 4.66,
            "_acceleration_x": 9.029,
            "_acceleration_y": 13.398,
            "_acceleration_z": 17.767,
            "_motion_detected": True,
            "tektelic_vivid_impact_detected": True,
        },
    ),
]


@pytest.mark.asyncio
@pytest.mark.parametrize("payload,sensors", parse_port_10_data)
async def test_parse_port_10(
    caplog_debug: pytest.LogCaptureFixture, payload: str, sensors: dict[str, object]
):
    device = Vivid()
    payload = payload.replace(" ", "")
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 10)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == sensors
    assert caplog_debug.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_10_not_implemented(
    caplog_debug: pytest.LogCaptureFixture,
):
    WARNING_MESSAGES = [
        "External connector analog value",
        "External connector relative count",
        "External connector total count",
        "Uncalibrated light intensity",
        "MCU temperature",
    ]

    device = Vivid()
    payload = "11020000 0F040000 120400000000 100200 0B670000".replace(" ", "")
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 10)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {}
    assert caplog_debug.record_tuples == [
        (
            "pyliblorawan.devices.tektelic.vivid",
            logging.WARNING,
            f"{warning_message} not implemented",
        )
        for warning_message in WARNING_MESSAGES
    ]


@pytest.mark.asyncio
async def test_parse_port_unknown(caplog_debug: pytest.LogCaptureFixture):
    device = Vivid()
    payload = "00"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 101)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {}
    assert caplog_debug.record_tuples == [
        (
            "pyliblorawan.devices.tektelic.vivid",
            logging.WARNING,
            'Unknown frame port "101"',
        )
    ]


@pytest.mark.asyncio
async def test_parse_unknown_channel_type(caplog_debug: pytest.LogCaptureFixture):
    device = Vivid()
    payload = "0000"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 10)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {}
    assert caplog_debug.record_tuples == [
        (
            "pyliblorawan.devices.tektelic.vivid",
            logging.ERROR,
            'Unknown data channel and type id "0000"',
        )
    ]
