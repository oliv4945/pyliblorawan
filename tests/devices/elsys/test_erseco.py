import logging

import pytest

from pyliblorawan.devices.elsys.erseco import ErsEco
from pyliblorawan.models import Uplink


@pytest.mark.asyncio
async def test_parse_port_102(caplog: pytest.LogCaptureFixture):
    device = ErsEco()
    payload = "0100ae024e040001070dd3"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {
        "_battery_level": 3.54,
        "_temperature": 17.4,
        "_humidity": 78,
        "_illuminance": 1,
    }

    assert caplog.record_tuples == []


@pytest.mark.asyncio
async def test_parse_port_unknown(caplog: pytest.LogCaptureFixture):
    device = ErsEco()
    payload = "000B374DB77A0400"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 101)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {}
    assert caplog.record_tuples == [
        (
            "pyliblorawan.devices.elsys.erseco",
            logging.WARNING,
            'Unknown frame port "101"',
        )
    ]


@pytest.mark.asyncio
async def test_parse_unknown_type(caplog: pytest.LogCaptureFixture):
    device = ErsEco()
    payload = "00"
    uplink = Uplink("0011223344556677", bytes.fromhex(payload), 5)

    await device.parse_uplink(uplink)

    assert uplink.sensors.__dict__ == {}
    assert caplog.record_tuples == [
        (
            "pyliblorawan.devices.elsys.erseco",
            logging.ERROR,
            'Unable to parse type "0"',
        )
    ]
