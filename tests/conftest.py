import asyncio
import logging

import pytest


@pytest.fixture(scope="module")
def event_loop():
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:
        loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture()
def caplog_debug(
    caplog: pytest.LogCaptureFixture,
):
    caplog.set_level(level=logging.DEBUG)
    return caplog


@pytest.fixture
def helium_uplink() -> dict:
    return {
        "app_eui": "00112233445566AA",
        "dev_eui": "00112233445566BB",
        "devaddr": "00112233",
        "fcnt": 19,
        "hotspots": [
            {
                "frequency": 912.2999877929688,
                "id": "11KHJW1ePkfhLYptMwKyDzn4opLc62daahbi35uxhH3wwi7p6xr",
                "name": "icy-fiery-hippo",
                "reported_at": 15868685461234,
                "rssi": -54,
                "snr": 9.800000190734863,
                "spreading": "SF9BW125",
                "status": "success",
            },
            {
                "frequency": 912.2999877929688,
                "id": "11ni2CACdVUAshDvXif2pYUduAsvoawoxvVub6aqgHjSNsPEapZ",
                "name": "cool-chiffon-hornet",
                "reported_at": 15868685461235,
                "rssi": -51,
                "snr": 9,
                "spreading": "SF9BW125",
                "status": "success",
            },
        ],
        "id": "7c523974-4ce7-4a92-948b-55171a6e4d77",
        "metadata": {
            "labels": [
                {
                    "id": "7d11aef4-4daf-4caf-b72a-f789b191ebe4",
                    "name": "TEST-LABEL",
                    "organization_id": "d724293a-89ca-4075-98e2-872bce31050b",
                }
            ]
        },
        "name": "Test Me",
        "payload": "/gDt",
        "port": 123,
        "reported_at": 15868685461234,
    }


@pytest.fixture
def orange_uplink() -> dict:
    return {
        "type": "dataMessage",
        "version": 1,
        "streamId": "urn:lora:00112233445566CC!uplink",
        "timestamp": "2023-12-05T11:12:26.072Z",
        "location": {
            "lat": 12.345,
            "lon": 67.89,
            "alt": 0.0,
            "accuracy": 123.4567,
            "provider": "lora",
        },
        "model": "lora_v0",
        "value": {"payload": "FE00ED"},
        "tags": [],
        "extra": {},
        "metadata": {
            "source": "urn:lo:nsid:lora:00112233445566CC",
            "group": {"id": "root", "path": "/"},
            "connector": "lora",
            "network": {
                "lora": {
                    "devEUI": "00112233445566CC",
                    "port": 123,
                    "fcnt": 79,
                    "missingFcnt": 2,
                    "rssi": -113.199997,
                    "snr": -6.25,
                    "esp": -120.37,
                    "sf": 9,
                    "frequency": 868.5,
                    "signalLevel": 4,
                    "ack": True,
                    "messageType": "CONFIRMED_DATA_UP",
                    "location": {
                        "lat": 12.345,
                        "lon": 67.89,
                        "alt": 0.0,
                        "accuracy": 123.4567,
                        "provider": "lora",
                    },
                    "gatewayCnt": 2,
                }
            },
        },
        "created": "2023-12-05T11:12:26.456Z",
    }


@pytest.fixture
def ttn_uplink() -> dict:
    return {
        "end_device_ids": {
            "device_id": "TEST-DEVICE",
            "application_ids": {"application_id": "TEST-APPLICATION"},
            "dev_eui": "feEDABCD00000002",
            "join_eui": "feEDABCD00000001",
        },
        "uplink_message": {"frm_payload": "/gDt", "f_port": 123},
    }
